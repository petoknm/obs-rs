extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    // Tell cargo to tell rustc to link the system obs
    // shared library.
    println!("cargo:rustc-link-lib=obs");

    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .blacklist_item("FP_(NAN|INFINITE|ZERO|(SUB)?NORMAL)")
        .generate()
        .expect("Unable to generate bindings");

    // Write the bindings to the $OUT_DIR/bindings.rs file.
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
