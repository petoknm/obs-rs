#[macro_use]
extern crate lazy_static;
extern crate obs_sys;

use obs_sys::*;

#[no_mangle]
pub extern fn obs_module_set_pointer(_m: *mut obs_module_t) {
}

#[no_mangle]
pub extern fn obs_module_load() -> bool {
    true
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
